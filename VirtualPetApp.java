import java.util.Scanner;

public class VirtualPetApp{
	
	public static void main(String[] args){
		Scanner reader = new Scanner(System.in);
		Crow[] murderOfCrows = new Crow[4];
		
		for(int i = 0; i<murderOfCrows.length; i++){
			murderOfCrows[i] = new Crow();
			
			System.out.println("it can fly (write true or false)");
			murderOfCrows[i].canFly = Boolean.parseBoolean(reader.nextLine());
			
			System.out.println("how many feathers does it have");
			murderOfCrows[i].numFeathers = Integer.parseInt(reader.nextLine());
			
			System.out.println("what does it eat");
			murderOfCrows[i].whatFood = reader.nextLine();
		}
		
		System.out.println(murderOfCrows[murderOfCrows.length-1].canFly);
		System.out.println(murderOfCrows[murderOfCrows.length-1].numFeathers);
		System.out.println(murderOfCrows[murderOfCrows.length-1].whatFood);
		
		murderOfCrows[0].judgeFood();
		murderOfCrows[0].noFlySad();
	}

}