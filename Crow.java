public class Crow{
	
	public boolean canFly;
	public int numFeathers;
	public String whatFood;
	
	public void judgeFood(){
		if(!(this.whatFood.equals("steak"))){
			System.out.println("Rethink your diet");
		}else{
			System.out.println("Keep it up, lookin good! ;)");
		}	
	}
	
	public void noFlySad(){
		if(!canFly){
			System.out.println("You're a bird, what you mean you can't fly -  get yourself together");
		}else{
			System.out.println("Good.");
		}
	}
	
}